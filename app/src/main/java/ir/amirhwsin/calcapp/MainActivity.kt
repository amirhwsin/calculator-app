package ir.amirhwsin.calcapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var num0: AppCompatTextView
    lateinit var num1: AppCompatTextView
    lateinit var num2: AppCompatTextView
    lateinit var num3: AppCompatTextView
    lateinit var num4: AppCompatTextView
    lateinit var num5: AppCompatTextView
    lateinit var num6: AppCompatTextView
    lateinit var num7: AppCompatTextView
    lateinit var num8: AppCompatTextView
    lateinit var num9: AppCompatTextView

    lateinit var dot: AppCompatTextView
    lateinit var sum: AppCompatTextView
    lateinit var mins: AppCompatTextView
    lateinit var equals: AppCompatTextView
    lateinit var ac: AppCompatImageView
    lateinit var clear: AppCompatTextView
    lateinit var multiply: AppCompatTextView
    lateinit var percent: AppCompatTextView
    lateinit var refresh: AppCompatImageView
    lateinit var divide: AppCompatTextView

    lateinit var operationTextView: AppCompatTextView
    lateinit var resultTextView: AppCompatTextView

    lateinit var calculator: Calculator
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createViews()
        calculator = Calculator()
//        calculator.appendSpecialChar(".")
//        calculator.calculate("1+2+3")
    }

    private fun createViews() {
        num0 = findViewById(R.id.num_0)
        num1 = findViewById(R.id.num_1)
        num2 = findViewById(R.id.num_2)
        num3 = findViewById(R.id.num_3)
        num4 = findViewById(R.id.num_4)
        num5 = findViewById(R.id.num_5)
        num6 = findViewById(R.id.num_6)
        num7 = findViewById(R.id.num_7)
        num8 = findViewById(R.id.num_8)
        num9 = findViewById(R.id.num_9)

        dot = findViewById(R.id.dot)
        sum = findViewById(R.id.sum)
        mins = findViewById(R.id.minus)
        equals = findViewById(R.id.equals)
        ac = findViewById(R.id.ac)
        clear = findViewById(R.id.clear)
        multiply = findViewById(R.id.multiply)
        percent = findViewById(R.id.percent)
        refresh = findViewById(R.id.refresh)
        divide = findViewById(R.id.divide)

        operationTextView = findViewById(R.id.operation_text_view)
        resultTextView = findViewById(R.id.temp_result_text_view)

        num0.setOnClickListener(this)
        num1.setOnClickListener(this)
        num2.setOnClickListener(this)
        num3.setOnClickListener(this)
        num4.setOnClickListener(this)
        num5.setOnClickListener(this)
        num6.setOnClickListener(this)
        num7.setOnClickListener(this)
        num8.setOnClickListener(this)
        num9.setOnClickListener(this)
        dot.setOnClickListener(this)
        sum.setOnClickListener(this)
        mins.setOnClickListener(this)
        equals.setOnClickListener(this)
        ac.setOnClickListener(this)
        clear.setOnClickListener(this)
        multiply.setOnClickListener(this)
        percent.setOnClickListener(this)
        refresh.setOnClickListener(this)
        divide.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.num_0 -> calculator.appendNumber("0")
            R.id.num_1 -> calculator.appendNumber("1")
            R.id.num_2 -> calculator.appendNumber("2")
            R.id.num_3 -> calculator.appendNumber("3")
            R.id.num_4 -> calculator.appendNumber("4")
            R.id.num_5 -> calculator.appendNumber("5")
            R.id.num_6 -> calculator.appendNumber("6")
            R.id.num_7 -> calculator.appendNumber("7")
            R.id.num_8 -> calculator.appendNumber("8")
            R.id.num_9 -> calculator.appendNumber("9")
            R.id.dot   -> calculator.appendSpecialChar(".")
            R.id.equals   -> calculator.calculate()
            R.id.minus   -> calculator.appendSpecialChar("-")
            R.id.divide   -> calculator.appendSpecialChar("/")
            R.id.multiply   -> calculator.appendSpecialChar("*")
            R.id.percent   -> calculator.appendSpecialChar("%")
            R.id.sum   -> calculator.appendSpecialChar("+")
            R.id.clear    -> calculator.clear()
            R.id.ac     -> calculator.clearOne()
        }
//        resultTextView.text = calculator.toString()
        calculator.to(resultTextView)
        calculator.to(operationTextView, calculator.previousCalculate)
    }
}
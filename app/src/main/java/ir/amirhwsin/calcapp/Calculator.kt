package ir.amirhwsin.calcapp

import android.content.Context
import android.content.res.TypedArray
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import java.lang.Exception
import java.math.BigDecimal

class Calculator() {

    private var operation: String = ""
    private var operators: MutableList<String> = mutableListOf("+","*","/","-","%","=")
    private var result: Boolean = false
    var previousCalculate = ""

    /**
     * Adds given number to operation
     * @see operation
     * @return bool
     */
    fun appendNumber(num: String) : Boolean {
        if (result) {
            previousCalculate = this.operation
            clear()
        }
        return if (num == "0" && this.operation == "") {
            false;
        }else {
            this.operation += num
            true;
        }
    }

    /**
     * Adds special character or operator
     * @see operation
     * @see operators
     */
    fun appendSpecialChar(char: String) {
        val isOperator: String ?= operators.find { it == char }
        if (isOperator == null) {
            if (this.operation.contains("([*+/\\-%=])".toRegex())) {
                val tp: Array<String> = this.operation.split("([*+/\\-%=])".toRegex()).toTypedArray()
                if (!tp[1].contains(".", ignoreCase = true) && this.operation != "") {
                    this.operation += "."
                }
            }
            if (!this.operation.contains(".", ignoreCase = true) && this.operation != "") {
                this.operation += "."
            }
        }else {
            if (!this.operation.contains("([*+/\\-%=])".toRegex()) && this.operation != "") {
                this.operation += char
            }
        }
    }

    /**
     * Clears operation
     * @see operation
     */
    fun clear() {
        this.operation = ""
        this.result = false
    }

    /**
     * Clears one char from the last of string
     * @see operation
     */
    fun clearOne() {
        if (this.operation != "" && this.operation.isNotEmpty()) {
            this.operation = this.operation.substring(0, this.operation.length - 1)
        }
    }

    /**
     * Calculate operation
     * @return Calculator
     */
    fun calculate(): Calculator {
        val tp: Array<String> = this.operation.split("([*+/\\-%=])".toRegex()).toTypedArray()
        this.previousCalculate = this.operation
        val re = Regex("([0-9])|[.]")
        if (tp.size < 2) {
            return this
        }
        when(re.replace(this.operation, "")){
            "+" -> this.operation = beatify(tp[0].toDouble() + tp[1].toDouble())
            "-" -> this.operation = beatify(tp[0].toDouble() - tp[1].toDouble())
            "*" -> this.operation = beatify(tp[0].toDouble() * tp[1].toDouble())
            "/" -> this.operation = beatify(tp[0].toDouble() / tp[1].toDouble())
            "%" -> this.operation = beatify(tp[0].toDouble() % tp[1].toDouble())
        }
        result = true
        return this
    }

    /**
     * Fetches operation or given string to AppCompatTextView
     * @see AppCompatTextView
     */
    fun to(textView: AppCompatTextView, w: String = "") {
        try {
            textView.text = if (w.isEmpty()) this.operation else w
        }catch (e: Exception) {
            Log.e("Calculator", "Unable to add result to view ", e)
        }
    }

    /**
     * Return operation as string
     */
    override fun toString(): String {
        return this.operation
    }

    /**
     * Remove decimals > 2
     * @see BigDecimal
     */
    fun beatify(number: Double) : String {
        return (BigDecimal(number).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString())
    }
}